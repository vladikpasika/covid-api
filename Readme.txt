1. docker build -t foo/app-api .
2. //docker run -d -p 3005:3005 --name node-app foo/node
docker run --rm -d -p 3005:3005 --name node-app-api foo/app-api


mutation RegionMutation($region: String, $date: String, $ill: Int, $cases: Int, $recovered: Int, $death: Int) {
  addReport(region: $region, date: $date,
        ill: $ill,
        cases: $cases,
        recovered: $recovered,
        death: $death) {
    region,
    date,
    ill,
    cases,
    recovered,
    death
  }
}


{
  "region": "KS",
  "date": "2020-04-16T19:37:46.820Z",
  "ill": 20,
  "cases": 40,
  "recovered": 60,
  "death": 50
}

$ mongo -u <your username> -p <your password> --authenticationDatabase <your database name>
### OR ###
$ mongo -u <your username> --authenticationDatabase <your database name>


mongo -u Wlad -p hhd45eds --authenticationDatabase Covid
mongodump -d Covid -u Wlad -p hhd45eds