import { IResolvers } from "graphql-tools";
import { AuthenticationError, ForbiddenError } from "apollo-server-express";

import * as bcrypt from "bcryptjs";
import { sign } from "jsonwebtoken";

import User, { IUser } from "./models/User";
import Region from "./models/Region";
import ReportsDate, { IReportsDate } from "./models/ReportsDate";
import { ACCESS_TOKEN_SECRET, REFRESH_TOKEN_SECRET } from "./constants";

function getFormattedDate(date: string): string {
  const formattedDate = new Date(date);
  formattedDate.setUTCHours(0, 0, 0, 0);
  return formattedDate.toISOString();
}

async function addReportDate(date: string) {
  const formattedDate = getFormattedDate(date);
  const currentDay = new Date(formattedDate);
  const nextDay = new Date(formattedDate);
  nextDay.setDate(currentDay.getDate() + 1);
  const isAlreadyDateExist = await ReportsDate.findOne({
    date: {
      $gte: currentDay,
      $lt: nextDay,
    },
  });
  if (isAlreadyDateExist) {
    return isAlreadyDateExist;
  }
  return await ReportsDate.create({ date: formattedDate });
}

export const resolvers: IResolvers = {
  SavedRegion: {
    region: async (region) => {
      const formattedDate = getFormattedDate(region.date);
      const currentDay = new Date(formattedDate);
      const nextDay = new Date(formattedDate);
      nextDay.setDate(currentDay.getDate() + 1);
      const existingReport = await Region.findOne({
        date: { $gte: currentDay, $lt: nextDay },
        region: region.region,
      });
      if (existingReport) {
        existingReport.overwrite(region);
        const updatedRecords = await existingReport.save();
        return updatedRecords;
      }
      const createdReport: IReportsDate = await Region.create(region);

      return createdReport;
    },
  },
  AreaChartMainData: {
    categories: async () => {
      const dates = await ReportsDate.find({}).sort({ date: 1 });
      return dates.map((category: any) => category.date);
    },
    reports: async () => {
      const reports: any = await Region.aggregate([
        { $group: { _id: "$date", cases: { $sum: "$cases" } } },
        { $sort: { _id: 1 } },
        { $project: { name: "$_id", data: "$cases" } },
      ]);
      return reports;
    },
  },
  AreaChartData: {
    categories: async () => {
      return await ReportsDate.find({}).sort({ date: 1 });
    },
    reports: async () => {
      const reports = await Region.aggregate([
        {
          $group: {
            _id: "$region",
            count: { $sum: 1 },
            reports: { $push: { date: "$date", cases: "$cases" } },
          },
        },
        { $unwind: { path: "$reports" } },
        { $sort: { "reports.date": 1 } },
        {
          $group: {
            _id: "$_id",
            data: {
              $push: "$reports.cases",
            },
          },
        },
        { $project: { name: "$_id", data: "$data" } },
      ]);
      return reports;
    },
  },
  Query: {
    me: async (_, __, { req }): Promise<IUser | null> => {
      if (!req.userId) {
        return null;
      }
      const user = await User.findById(req.userId);
      return user;
    },
    regions: async (_, { date }) => {
      return await Region.find({
        date: { $gte: new Date(getFormattedDate(date)) },
      });
    },
    lastReportsDate: async function () {
      const result = (
        await ReportsDate.find({}).sort({ date: -1 }).limit(1)
      ).pop();
      return result;
    },
    aggregatedDataForMainDynamicChart: (...rest) => {
      return rest;
    },
    aggregatedDataForDynamicChart: (...rest) => {
      return rest;
    },
  },
  Mutation: {
    register: async (_, { email, password }) => {
      const user: IUser | null = await User.findOne({ email });
      if (user) {
        throw new ForbiddenError("User alredy exists");
      }
      const hashedPassword = await bcrypt.hash(password, 10);
      await User.create({
        email,
        password: hashedPassword,
      });

      return true;
    },
    login: async (_, { email, password }, { res }) => {
      const user: IUser | null = await User.findOne({ email });
      if (!user) {
        return null;
      }
      if (user && user.password) {
        const valid = await bcrypt.compare(password, user.password);
        if (!valid) {
          return null;
        }
      }

      const refreshToken = sign(
        { userId: user.id, count: user.count },
        REFRESH_TOKEN_SECRET,
        {
          expiresIn: "7d",
        }
      );
      const accessToken = sign({ userId: user.id }, ACCESS_TOKEN_SECRET, {
        expiresIn: "15min",
      });

      res.cookie("refresh-token", refreshToken);
      res.cookie("access-token", accessToken);

      return user;
    },
    addReports: async function (_: any, { regions }, { req }) {
      if (!req.userId) {
        throw new AuthenticationError("You must be logged in");
      }
      return regions;
    },
    setLastReportsDate: async function (_: any, { date }, { req }) {
      if (!req.userId) {
        throw new AuthenticationError("You must be logged in");
      }
      return await addReportDate(date);
    },
  },
};
