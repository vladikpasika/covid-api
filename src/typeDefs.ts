import { gql } from "apollo-server-express";

export const typeDefs = gql`
  type DynamicChartReport {
    data: Int
    name: String
  }
  type Categories {
    date: String
  }
  type AggregatedData {
    data: Int
  }
  type AreaChartMainData {
    categories: [String]
    reports: [DynamicChartReport]
  }
  type AggregatedReports {
    name: String
    data: [Int]
  }
  type AreaChartData {
    categories: [Categories]
    reports: [AggregatedReports]
  }
  input LastDate {
    date: String!
  }
  type SavedRegion {
    region(region: RegionInput): Region
  }
  type Region {
    region: String!
    date: String!
    cases: Int!
    ill: Int
    recovered: Int
    death: Int
  }
  input RegionInput {
    region: String!
    date: String!
    cases: Int!
    ill: Int
    recovered: Int
    death: Int
  }
  type User {
    id: ID!
    email: String!
  }
  type Query {
    me: User
    regions(date: String): [Region]
    lastReportsDate: LastDateResult
    aggregatedDataForMainDynamicChart: AreaChartMainData
    aggregatedDataForDynamicChart: AreaChartData
  }
  type LastDateResult {
    date: String!
  }
  type Mutation {
    register(email: String!, password: String!): Boolean!
    login(email: String!, password: String!): User
    addReports(regions: [RegionInput]): [SavedRegion]
    setLastReportsDate(date: String!): LastDateResult
  }
`;
