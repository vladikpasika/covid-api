import mongose, { Schema } from "mongoose";

export interface IRegion extends mongose.Document {
  region: string;
  date: string;
  cases: number;
  ill?: number;
  recovered?: number;
  deth?: number;
}

export const RegionSchema = new Schema({
  region: { type: String, required: true },
  date: { type: Date, required: true },
  cases: { type: Number, required: true },
  ill: Number,
  recovered: Number,
  death: Number,
});

export default mongose.model("regions", RegionSchema, "regions");
