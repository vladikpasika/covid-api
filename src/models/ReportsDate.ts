import mongose, { Schema } from "mongoose";

export interface IReportsDate extends mongose.Document {
  date?: Date;
}

export const ReportsDateSchema = new Schema({
  date: { type: Date, required: true }
});

export default mongose.model("reportsDate", ReportsDateSchema, "reportsDate");
