import mongose, { Schema, Document } from "mongoose";

export interface IUser extends Document {
  count?: number;
  email?: string;
  password?: string;
}

export const User = new Schema({
  count: Number,
  email: String,
  password: String,
});

export default mongose.model("users", User, "users");
