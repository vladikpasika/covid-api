import mongoose from "mongoose";

interface confOptions {
  autoIndex: boolean;
  user?: string;
  pass?: string;
}

function getOptions(isAuth: boolean): confOptions {
  const conf: confOptions = {
    autoIndex: false,
  };
  if (isAuth) {
    (conf.user = "Wlad"), (conf.pass = "hhd45eds");
  }
  return conf;
}

class MongoInstance {
  static tries: number = 0;
  static mongooseConnect(): void {
    mongoose.connect(
      `${
        process.env.NODE_ENV === "development"
          ? "mongodb://localhost:27017/Covid"
          : "mongodb://mongo:27017/Covid"
      }`,
      {
        useNewUrlParser: true,
        ...getOptions(!!(process.env.NODE_ENV === "production")),
      }
    );
  }
  static instance: object | null;
  constructor(instance?: object) {
    MongoInstance.instance = instance || null;
  }

  static async getInstance() {
    if (!MongoInstance.instance) {
      this.mongooseConnect();
      mongoose.connection
        .once("open", () => {
          console.log("Подключились к базе данных");
        })
        .on("error", (err: any) => {
          console.error("Connection error:", err);
          if (this.tries < 5) {
            this.tries = this.tries++;
            setTimeout(() => {
              this.mongooseConnect();
            }, 7000);
          }
        });
    }
    return new MongoInstance(mongoose);
  }
}

export default MongoInstance;
