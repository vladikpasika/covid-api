import { verify } from "jsonwebtoken";
import express from "express";

import { ACCESS_TOKEN_SECRET } from "./constants";

export default function checkAuth(
  req: express.Request,
  _: any,
  next: express.NextFunction
) {
  const accessToken = req.cookies["access-token"];
  try {
    const data = verify(accessToken, ACCESS_TOKEN_SECRET) as any;
    if (data) {
      (req as any).userId = data.userId;
    }
  } catch (err) {}
  next();
}
