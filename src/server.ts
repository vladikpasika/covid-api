import "reflect-metadata";
import { ApolloServer } from "apollo-server-express";
import express from "express";
import cookieParser from "cookie-parser";

import { typeDefs } from "./typeDefs";
import { resolvers } from "./resolves";
import MongoDBConnection from "./config/MongoDBConnection";
import checkAuth from "./checkAuth";

const startServer = async () => {
  const server = new ApolloServer({
    typeDefs,
    resolvers,
    introspection: true,
    playground: true,
    context: ({ req, res, next }: any) => {
      return { req, res, next };
    },
  });

  await MongoDBConnection.getInstance();

  const app = express();

  app.use(cookieParser());
  app.use(checkAuth);
  server.applyMiddleware({ app, path: "/api" }); // app is from an existing express app

  app.listen({ port: 3005 }, () =>
    console.log(`🚀 Server ready at http://localhost:3005${server.graphqlPath}`)
  );
};

startServer();
